WGBlockRestricter
=================

WorldGuard extension to black/whitelist block types in specific regions.

Dependencies: WorldGuard, WGCustomFlags, WorldEdit  
Tested on: Paper 1.15.2 (Spigot 1.15.2 fork)